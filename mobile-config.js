App.info({
  id:          'com.carbomen.ph',
  name:        'Prueba Hidraúlica',
  description: 'Muestra información sobre los procesos y los cilindros de los clientes',
  version:     '0.1.0'
})


App.icons({
  //'android_ldpi': 'public/app/res/mipmap-mdpi/ic_launcher.png',
  'android_mdpi': 'public/app/res/mipmap-mdpi/ic_launcher.png',
  'android_hdpi': 'public/app/res/mipmap-hdpi/ic_launcher.png',
  'android_xhdpi': 'public/app/res/mipmap-xhdpi/ic_launcher.png',
  'android_xxhdpi': 'public/app/res/mipmap-xxhdpi/ic_launcher.png',
  'android_xxxhdpi': 'public/app/res/mipmap-xxxhdpi/ic_launcher.png'

})



App.launchScreens({
  //'android_ldpi_landscape':  'public/app/screens/ldpi-l.9.png',
  'android_mdpi_portrait':   'public/app/res/drawable-mdpi/background.9.png',
  //'android_mdpi_landscape':  'public/app/screens/mdpi-l.9.png',
  'android_hdpi_portrait':   'public/app/res/drawable-hdpi/background.9.png',
  //'android_hdpi_landscape':  'public/app/screens/hdpi-l.9.png',
  'android_xhdpi_portrait':  'public/app/res/drawable-xhdpi/background.9.png'
  //'android_xhdpi_landscape': 'public/app/screens/xhdpi-l.9.png'
  //'android_xxhdpi_portrait':  'public/app/screens/xxhdpi.9.png'
  //'android_xxxhdpi_portrait':  'public/app/screens/xxxhdpi.9.png',
})

App.accessRule('http://*')
App.accessRule('https://*')