/**
 * Created by mario on 14/01/16.
 */
moment.locale('es')
console.log('moment.locale',moment.locale())


document.title='Prueba Hidraúlica'

$(document).on('backbutton', function (event) {
    var routeName = Router.current() && Router.current().route.getName()
    var $previous = $('[data-previous]')
    var $cancel   = $('[data-cancel]')

    if (routeName === 'dashboard' || routeName === 'root') {
        event.preventDefault()
        navigator.app.exitApp()
    } else if ($previous.length) {
        $previous.first().click()
    } else if ($cancel.length) {
        $cancel.first().click()
    } else {
        Router.go('dashboard')
    }
})
