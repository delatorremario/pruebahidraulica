/**
 * Created by mario on 10/08/15.
 */

accountsUIBootstrap3.setLanguage('es');


Accounts.ui.config({
    forceEmailLowercase: true
    //forbidClientAccountCreation : false//,
    //forceUsernameLowercase: true,
    //forcePasswordLowercase: true
});

Accounts.ui.config({
    forceEmailLowercase: true,
    requestPermissions: {},
    extraSignupFields: [{
        fieldName: 'name',
        fieldLabel: 'Nombre y Apellido',
        inputType: 'text',
        visible: true,
        validate: function(value, errorFunction) {
            if (!value) {
                errorFunction("Por favor ingrese su nombre");
                return false;
            } else {
                return true;
            }
        }
    }/*, {
        fieldName: 'terms',
        fieldLabel: 'Acepta términos y condiciones.',
        inputType: 'checkbox',
        visible: true,
        saveToProfile: false,
        validate: function(value, errorFunction) {
            if (value) {
                return true;
            } else {
                errorFunction('Debe aceptar los términos y condiciones.');
                return false;
            }
        }
    }*/
    ]
});

