/**
 * Created by mario on 06/08/15.
 */
alertify.defaults = {
    // dialogs defaults
    modal:true,
    basic:false,
    frameless:false,
    movable:true,
    resizable:true,
    closable:true,
    closableByDimmer:true,
    maximizable:true,
    startMaximized:false,
    pinnable:true,
    pinned:true,
    padding: true,
    overflow:true,
    maintainFocus:true,
    transition:'zoom',
    autoReset:true,

    // notifier defaults
    notifier:{
        // auto-dismiss wait time (in seconds)
        delay:5,
        // default position
        position:'bottom-right'


    },

    // language resources
    glossary:{
        // dialogs default title
        title:'',
        // ok button text
        ok: 'SI',
        // cancel button text
        cancel: 'Cancelar'
    },

    // theme settings
    theme:{
        // class name attached to prompt dialog input textbox.
        input:'form-control',
        // class name attached to ok button
        ok:'btn btn-primary',
        // class name attached to cancel button
        cancel:'btn btn-danger'
    }
};