/**
 * Created by mario on 17/03/16.
 */
/*$('inputs').keydown(function (event) {
    console.log('inputs enter')
    if (event.which == 13) {
        event.preventDefault();
        var $this = $(event.target);
        var index = parseFloat($this.attr('data-index'));
        $('[data-index="' + (index + 1).toString() + '"]').focus();
    }
});
*/

/**/
// register jQuery extension
jQuery.extend(jQuery.expr[':'], {
    focusable: function (el, index, selector) {
        return $(el).is('a, button, :input, [tabindex]');
    }
});

$(document).on('keypress', 'input,select', function (e) {
    //console.log('e',e.currentTarget.id);
    if (e.which == 13 && e.currentTarget.id!='password' && e.currentTarget.id!='filtro') {
        e.preventDefault();
        //console.log('entra');
        // Get all focusable elements on the page
        var $canfocus = $(':focusable');
        var index = $canfocus.index(this) + 1;
        if (index >= $canfocus.length) index = 0;
        $canfocus.eq(index).focus();
    }
});