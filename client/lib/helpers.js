Template.registerHelper('isCordova', function () {
  return Meteor.isCordova
})

Template.registerHelper('connected', function () {
  return Meteor.status().status === 'connected'
})

Template.registerHelper('l', function (date, format) {
  if (typeof format !== 'string') format = 'L'

  return date && moment(date).format(format)
})

