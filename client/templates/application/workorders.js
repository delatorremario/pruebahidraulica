/**
 * Created by mario on 11/02/16.
 */

filtroWO =new ReactiveVar('')
limitWO = new ReactiveVar(10)
selectedcylinder = new ReactiveVar(null)



Template.workordersnew.helpers({
   selectedcylinder:function(){
            return Cylinders.findOne({_id:selectedcylinder.get()})
    }
})
Template.workorders.events({
    'click #newWorkOrder':function(){
        //e.preventDefault()
        selectedcylinder.set(null)
    }
})

Template.workorder.helpers({
    tasks:function(){
        return _.map(this.tasks,function(doc){
            return {task:doc}
        })
    },
    task_state:function(){
        //console.log('this',this,Template.parentData())
        var finished = Template.parentData().tasks_finished?Template.parentData().tasks_finished:[]
        var task = this.task

        return finished.indexOf(task)>-1 && 'label label-success' || 'label label-info'


    },
    createdAt: function () {
        return moment(this.createdAt).format('ddd DD/MMM HH:mm')
    },
    cylinder_number:function(){
        return Cylinders.findOne({_id:this.cylinderId}).cylinder_number || ''
    },
    cylinders:function(){
        return Cylinders.find({_id:this.cylinderId})
    },
    customer:function(){
        //console.log('customer',Customers.findOne({_id:this.customerId}))

        return Cylinders.findOne({_id:this.cylinderId}) && Customers.findOne({_id:Cylinders.findOne({_id:this.cylinderId}).customerId}).name || ''
    }
})

var visual_inspection = new ReactiveVar(false)

Template.workordersedit.onCreated=function(){
    visual_inspection.set(false)
}



Template.workordersedit.helpers({
    omitFields:function(){
        var visual = visual_inspection.get()
        var omitFields = '_id,userId,createdAt,companyId,updatedAt,workorder_number,cylinderId,tasks,confirm_date,tasks_finished,stamp,sent,due_date,approved' //,updatedAt,numero_solicitud,comentarios,inicio_proceso,confirma_proceso'
                        
        return visual && omitFields || omitFields + ',roundness_1,roundness_2,roundness_3,roundness_4,roundness_5,roundness_6,'+
                                                    'work_pressure,mass,mass_control,waterfilled_cylinder,burette_reading_max,temperature,'+
                                                    'burette_reading_final,thickness_1,thickness_2,thickness_3,thickness_4,variation_thread'
    },
    show_ph:function(){
      return this.item.tasks && _.indexOf(this.item.tasks,'PH')>=0
    },
    confirm_date:function(){
        return this.confirm_date && moment(this.confirm_date).format('ddd DD MMMM YYYY HH:mm') || false
    },
    due_date:function(){
        return this.due_date && moment(this.due_date).format('DD-MMM-YYYY') || false
    },
    tasks:function(){
        return _.map(this.item.tasks,function(doc){
            return {task:doc}
        })
    },
    task_state:function(){

        var finished = Template.parentData().item.tasks_finished?Template.parentData().item.tasks_finished:[]
        var task = this.task

        return finished.indexOf(task)>-1 && 'label label-success' || 'btn btn-primary'


    },
    createdAt:function(){
        return moment(this.createdAt).format("ddd DD/MM HH:mm")
    },
    updatedAt:function(){
        return moment(this.updatedAt).format("ddd DD/MM HH:mm")
    },
    cylinder_volume: function () {
        return myworkorder.get().cylinder_volume()
    },
    cm3_reading_max:function(){
        return myworkorder.get().cm3_reading_max();//this.cylinder.capacity,this.burette_reading_max).toFixed(3)
    },
    cm3_reading_final:function(){
        return myworkorder.get().cm3_reading_final();//this.cylinder.capacity,this.burette_reading_final).toFixed(3)
    },
    balancing_factor:function(){
         return myworkorder.get().balancing_factor();//this.temperature,this.cylinder.test_pressure)

    },
    total_deformation:function(){

        return myworkorder.get().total_deformation()
    },
    deformation_test:function(){
        return myworkorder.get().deformation_test()

    },
    thickness_test:function(){
        return myworkorder.get().thickness_test()
    },
    roundness_test:function(){
        return myworkorder.get().roundness_test()
    },
    visual_inspection_class:function(){
        return visual_inspection.get() && "btn-success fa fa-thumbs-o-up" || "btn-danger fa fa-thumbs-o-down"
    },
    visual_inspection:function(){
        return visual_inspection.get()
    }

})
Template.workordersedit.events({
    'click #visual_inspection':function(e){
        e.preventDefault()
        visual_inspection.set(!visual_inspection.get())
        return visual_inspection.get() && "btn-success fa fa-thumbs-o-up" || "btn-danger fa fa-thumbs-o-down"
    },
    'click .task':function(e,t){
        e.preventDefault()
        Meteor.call('updateTask',Template.parentData().item._id,this.task,function(err){
            if(err)alertify.error(err)
        })

    },
    'click #confirmWorkOrder ':function(e){
        e.preventDefault()
        var itemId=this._id
        var message="¿Quiere confirmar los datos de la Órden de Trabajo?"
        var vence = $('#vence').val();
        //console.log('this',this)
        var tasks = this.tasks


        alertify.confirm( message, function (confirm) {
            if (confirm) {
                //console.log('rol',rol)
                var approved= visual_inspection.get() & myworkorder.get().roundness_test() &
                    myworkorder.get().thickness_test() &
                    myworkorder.get().deformation_test()
                //console.log('myworkorder.get().cylinder',myworkorder.get().data.cylinder)
                var ph = _.indexOf(tasks,'PH')>=0?true:false
                Meteor.call('confirmWorkOrder',itemId,ph,vence,approved===1?true:false
                    ,myworkorder.get().data.cylinderId
                    ,visual_inspection.get()
                    ,function(err){
                    if(err) alertify.error(err.reason)
                })
            }
        }).set('defaultFocus', 'cancel');
    }
})



Template.cylinderdata.helpers({
    customer:function(){
        //console.log('customer',Customers.findOne({_id:this.customerId}))

        return Cylinders.findOne({_id:this._id}) && Customers.findOne({_id:Cylinders.findOne({_id:this._id}).customerId}).name || ''
    }
})

Template.workordersearch.helpers({
    allowEdit:function(){
            return true
    },
    countTotal:function(){

        return WorkOrders.find().count()
    },

    notfound:function(){

        var count = WorkOrders.find().count()
        var filtro = Number(filtroWO.get())
        if(count==0 && filtro!='') return true
        else return false
    },
    mostrarBtnMas:function(){
        var count = Cylinders.find().count()
        var limit = Number(limitWO.get())
        //console.log('count,limit',count,limit)
        if(count>=limit) return true
        else return false
    }
})
Template.workordersearch.events({
    'click #quieromas':function(){
        limitWO.set(limitWO.get() + 10)
    },
    'keypress #filtro': function (event,template) {
        if (event.which === 13) {
            var filtro =document.getElementById('filtro').value
            //console.log('filtro',filtro)
            var anterior = filtroWO.get()
            if(filtro && filtro===anterior)
                limitWO.set(limitWO.get() + 10)
            else
                limitWO.set( 10)

            filtroWO.set(filtro)
        }
    },
    'change input.stamp':function(e,t){
        e.preventDefault();

        Meteor.call('stampWorkOrder',this._id, e.currentTarget.value,function(error){
            if(error)alertify.error(error);
        })

    },
    'click .tocertificated':function(e){
        e.preventDefault()
        Meteor.call('certificatedWorkOrder',this._id,function(error){
            if(error)alertify.error(error);
        })
    },
    'click .send':function(e){
        e.preventDefault()
        Meteor.call('generarpdf',function(err,data){
            console.log('err',err,'data',data);
        })
    }
})



Template.workorders.events({
    'click #delete': function (e) {
        e.preventDefault()
        var itemId=this._id
        var message="¿Quiere eliminar la Orden de Trabajo?"
        alertify.confirm( message, function (confirm) {
            if (confirm) {
                //console.log('rol',rol)
                Meteor.call('deleteWO',itemId,function(err){
                    if(err) alertify.error(err)
                    //else alertify.success('Eliminado');
                    //else alertify.success('Realizado');var
                })
            }
        }).set('defaultFocus', 'cancel');
    }
})

AutoForm.addHooks('workordersnew', {
    before: {
        insert: function (doc) {

            setTimeout(function(){
                Router.go('/workorders');
            },200)

            return _.extend(doc,{_id:Random.id()})//,companyId:usr.companies[0]._id})
        },
        update:function(doc){
            /*//console.log('docedit',doc)
            setTimeout(function(){
                Router.go('/workorders');
            },200)*/

            return doc
        }
    },
    onSuccess:function(operation, result){

    },
    onError: function(formType, error) {

        if(error.error===409)
            alertify.error("Código Duplicado")
        else
            alertify.error(error.message   )

    }

})
