/**
 * Created by mario on 06/07/15.
 */

Template.user.helpers({

    nombre:function(){
        console.log('this',this)
       return this.profile.name
    },

    rolesPorUsuario:function(){
        var usr = this
        var roles = Roles.getRolesForUser(usr)
        var rolesObj = roles.map(function(value,index){
            return {name:value,usuario:usr}
        })
        return rolesObj


    }

})
Template.users.events({
    'click .addRol': function () {
        var usrId = document.getElementById('selectUser').value
        var rol = document.getElementById('selectRol').value

        var misroles=[rol]

        Meteor.call('addUserToRol',usrId,misroles,function(err){
            if(err) alertify.error(err.message)
            //else alertify.success('Realizado');
        })
    },
    'click .addComp': function () {
        var usrId = document.getElementById('selectUser').value
        var companyId = document.getElementById('selectCompany').value


        Meteor.call('addCompanyToUsr',usrId,companyId,function(err){
            if(err) alertify.error(err.reason)
            //else alertify.success('Realizado');
        })
    }
})

Template.user.events({


    'change .profileName':function(e,t){
        e.preventDefault()
        var id = this._id
        var nombre = document.getElementById('user_'+id).value
        Meteor.call('cambiarNombre',id,nombre,function(err){
            if(err) alertify.error(err.message)
            else alertify.success('Realizado');
        })
    },
    'click .verificar':function(e,t){
        e.preventDefault()
        var id = t.data._id
        Meteor.call('verificarEmail',id,function(err){
            if(err) alertify.error(err.message)
            else alertify.success('Realizado');
        })
    },
    'click #removeUser':function(e){
        e.preventDefault()
        var id = this._id
        var message="¿Quiere eliminar el Usuario?"
        alertify.confirm( message, function (confirm) {
            if (confirm) {

                Meteor.call('removeUser',id,function(err){
                    if(err) alertify.error(error)
                    else alertify.success('Eliminado');
                })
            } else {
                //after clicking Cancel
            }
        }).set('defaultFocus', 'cancel');

    },

    'click #quitarUsuarioASucursal':function(e){
        e.preventDefault()
        var sucId = selectedSucursalId.get()
        Meteor.call('quitarUsuarioAlaSucursal',sucId,this._id,function(err,val){
            if(err) alertify.error(err.message)
            else alertify.success('Realizado');

        })
    },
    'click #agregarUsuarioASucursal':function(e){
        e.preventDefault()
        var sucId = selectedSucursalId.get()
        Meteor.call('agregarUsuarioAlaSucursal',sucId,this._id,function(err,val){
            if(err) alertify.error(error)
            else alertify.success('Realizado');

        })
    },
    'click #removeUserFromRol':function(e){
        e.preventDefault()
        var usr = this.usuario
        var rol = this.name

        var misroles=[rol]
        Meteor.call('removeUserFromRol',usr,misroles,function(err){
            if(err) alertify.error(err.message)
            //else alertify.success('Realizado');
        })
    },
    'click .removeUserFromServicio':function(e,t){
        e.preventDefault()
        var usrId = t.data._id
        var servicio = this
        console.log('usr',usrId)
        console.log('servicio',servicio)

        Meteor.call('removeUserFromServicio',usrId,servicio,function(err){
            if(err) alertify.error(err.message)
            //else alertify.success('Realizado');
        })
    }
})