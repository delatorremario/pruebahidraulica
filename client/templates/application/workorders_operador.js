Template.workordersoperador.events({
    'change #codebar':function(e){
        e.preventDefault()
        var $number = $(e.currentTarget)
        var cylinder = Cylinders.findOne({cylinder_number:$number.val()})
        var order = cylinder && WorkOrders.findOne({cylinderId:cylinder._id},{sort:{createdAt:-1}})
        var params ={_id:order._id}


        Router.go('workordersedit',params)
    }
})