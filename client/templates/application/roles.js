/**
 * Created by mario on 03/08/15.
 */
Template.rol.events({

})
Template.roles.events({
    'click #agregarRol':function(e,t){
        e.preventDefault
        var rol =  document.getElementById('rol').value
        Meteor.call('createRole',rol,function(err){
            if(err) alertify.error(err)
            //else alertify.success('Realizado');
        })

    },
    'click #removeRol': function (e,t) {
        e.preventDefault()

        var rol = this
        var message="¿Quiere eliminar el Rol?"
        alertify.confirm( message, function (confirm) {
            if (confirm) {
                //console.log('rol',rol)
                Meteor.call('removeRol',rol,function(err){
                    if(err) alertify.error(err.message)
                    else alertify.success('Eliminado');
                    //else alertify.success('Realizado');var
                })
            }
        }).set('defaultFocus', 'cancel');


    }
})