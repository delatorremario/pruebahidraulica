/**
 * Created by mario on 11/04/16.
 */
Template.micilindro.helpers({

    style_approved:function(){
        return this.makeph && this.approved && 'success'
            || this.makeph && !this.approved && 'danger' || 'default'
    },
    style_due_date:function(){
        return moment(this.due_date)>moment(new Date()) && moment(this.due_date)<=moment(new Date()).add(1,'month') && 'warning'
            || moment(this.due_date)>moment(new Date()) && 'primary' || 'danger'
    },
    due_date:function(){
        return moment(this.due_date).format('MMM-YY')
    },
    mostrarBtnMas:function(){
        var count = Cylinders.find().count()
        var limit = Number(limitTubes.get())
        //console.log('count,limit',count,limit)
        if(count>=limit) return true
        else return false
    }
})
Template.micilindro.events({
    'click #btncomment':function(e){
        e.preventDefault()
        $("#"+this._id).slideToggle(200)
    },
    'click .sendmsg':function(e){
        var msg =$('#newmsg' + this._id).val()
        if(msg){
            var mensage = {
                user_name : Meteor.user().profile.name,
                comment:msg,
                date: new Date()
            }
            Meteor.call('addcomment',this._id,mensage,function(err){
                if(err)alertify.error(err.reason)
            })
        }

    }
})

Template.micilindrocomment.helpers({
    time:function(){
        return moment(this.date).format('HH:mm')
    }
})