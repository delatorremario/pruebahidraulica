/**
 * Created by mario on 30/01/16.
 */


filtroTubes = new ReactiveVar('')
limitTubes = new ReactiveVar(10)

Template.cylinder.helpers({
    lastph:function(){
        var lastworkorder = WorkOrders.findOne({cylinderId:this._id},{sort:{createdAt:-1}},{reactive:true})
        if(lastworkorder){
            //console.log('lastworkorder',lastworkorder);
            var lastph ={
                fecha:lastworkorder.confirm_date?moment(lastworkorder.confirm_date).format('DD-MM-YYYY'):( lastworkorder.updatedAt ?'Actualizado ' + moment(lastworkorder.updatedAt).format('HH:mm') :'...en espera'),
                style:lastworkorder.due_date?(lastworkorder.approved===true?'success':'danger'):'warning',
                due_date:lastworkorder.due_date?moment(lastworkorder.due_date).format('DD-MM-YYYY'):'',
                approved:lastworkorder.approved
            }
            //console.log('lastph',lastph)
            return lastph
        }
    }


})
/*Template.cylindersearch.rendered=function(){
    $('#filtro').focus().select();
}*/
Template.cylindersearch.helpers({
    addToOrder:function(){
            //console.log('parent',Template.instance().view.parentView.parentView.name)
            if(Template.instance().view.parentView.parentView.name=='Template.workordersnew')
                return true
    },
    allowEdit:function(){
        //console.log('parent',Template.instance().view.parentView.name)
        if(Template.instance().view.parentView.name=='Template.cylinders')
            return true
    },
    customer:function(){
     //console.log('customer',Customers.findOne({_id:this.customerId}))
     return Customers.findOne({_id:this.customerId}) || ''
     },

    countTotal:function(){

        return Cylinders.find().count()
    },

    notfound:function(){

        var count = Cylinders.find().count()
        var filtro = Number(filtroTubes.get())
        if(count==0 && filtro!='') return true
        else return false
    },
    mostrarBtnMas:function(){
        var count = Cylinders.find().count()
        var limit = Number(limitTubes.get())
        //console.log('count,limit',count,limit)
        if(count>=limit) return true
        else return false
    }
})
Template.cylindersearch.events({
    'click #quieromas':function(){
        limitTubes.set(limitTubes.get() + 10)
    },
    'keypress #filtro': function (event) {
        if (event.which === 13) {
            var filtro =document.getElementById('filtro').value;
            var anterior = filtroTubes.get();
            if(filtro && filtro===anterior)
                limitTubes.set(limitTubes.get() + 10);
            else
                limitTubes.set(10);
            filtroTubes.set(filtro);
            $('#filtro').focus().select();
        }
    },

    'click #addTubeToOrder':function(event){
        event.preventDefault()
        //console.log('contexto.data',contexto.data.item._id)
        var item = this
        selectedcylinder.set(item._id)

    },
    'click #printBarCode':function(e){
        e.preventDefault()
        $("#bcTarget").barcode(this.cylinder_number, "code128");
        $( ".printable" ).print();
    },
    'click .cylinder':function(e) {
        e.preventDefault()
        pushCylinderid(this._id);
    }
})

function pushCylinderid(cylinderId) {
    var cylinders = cylindersid.get()?cylindersid.get():[];
    cylinders.push(cylinderId);
    cylindersid.set(cylinders);
}

Template.cylinders.events({
    'click #deleteTube': function (e) {
        e.preventDefault()
        var itemId=this._id
        var message="¿Quiere eliminar el Cilindro?"
        alertify.confirm( message, function (confirm) {
            if (confirm) {
                //console.log('rol',rol)
                Meteor.call('deleteTube',itemId,function(err){
                    if(err) alertify.error(err.reason)
                    //else alertify.success('Eliminado');
                    //else alertify.success('Realizado');var
                })
            }
        }).set('defaultFocus', 'cancel');
    }
})

AutoForm.addHooks('cylindersnew', {
    before: {
        insert: function (doc) {

            /*setTimeout(function(){
                Router.go('/cylinders');
            },200)*/

            return _.extend(doc,{_id:Random.id(),customerId:selectedcustomerid.get()})
        },
        update:function(doc){
            //console.log('docedit',doc)
            setTimeout(function(){
                Router.go('/cylinders');
            },200)

            return doc
        }
    },
    onSuccess:function(operation, result){
        console.log(operation,result)
        if(operation==='insert') {
            pushCylinderid(result);

        }
    },
    onError: function(formType, error) {

        if(error.error===409)
            alertify.error("Código Duplicado")
        else
            alertify.error(error.message   )

    }

})
