/**
 * Created by mario on 01/02/16.
 */

searchcustomername = new ReactiveVar('');
searchcustomerlimit = new ReactiveVar(10);

Template.customersearch.rendered=function(){
    $('#filtro').focus().select();
}

Template.customersearch.helpers({

    mostrarBtnMas:function(){
        var count = Customers.find({},{sort:{name:1}},{limit:searchcustomerlimit.get(),reactive:true}).count()
        var limit = Number(searchcustomerlimit.get())
        //console.log('count,limit',count,limit)
        if(count>=limit) return true
        else return false
    }
})

Template.customersearch.events({
    'click #quieromas':function(){
        searchcustomerlimit.set(searchcustomerlimit.get() + 10)
    },
    'keypress #filtro': function (event,template) {
        if (event.which === 13) {
            var filtro =document.getElementById('filtro').value;
            var anterior = searchcustomername.get();
            if(filtro && filtro===anterior)
                searchcustomerlimit.set(searchcustomerlimit.get() + 10);
            else
                searchcustomerlimit.set(10);

            searchcustomername.set(filtro);
            $('#filtro').focus().select()
        }
    },
    'click .customer':function(e) {
        e.preventDefault()
        selectedcustomerid.set(this._id)
    }
})


AutoForm.addHooks('customeredit', {
    before: {
        insert: function (doc) {

            var usr = Meteor.user()
           /* setTimeout(function(){
                Router.go('/customers');
            },200)*/

            return _.extend(doc,{_id:Random.id()})//,companyId:usr.companies[0]._id})
        },
        update:function(doc){
            console.log('doc',doc)
            setTimeout(function(){
                Router.go('/customers');
            },200)
            return doc
        }
    },
    onSuccess:function(operation, result){
        console.log(operation,result)
       if(operation==='insert') selectedcustomerid.set(result)
    },
    onError: function(formType, error) {

        if(error.error===409)
            alertify.error("Código Duplicado")
        else
            alertify.error("Error:"+ error.reason   )

    }

})

