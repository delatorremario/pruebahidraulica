/**
 * Created by mario on 04/04/16.
 */



Template.certificatepdf.helpers({
    logo:function(){
        return logo;
    },
    veritas:function(){
        return veritas;
    },
    opds:function(){
        return opds;
    },
    fecha:function(){
        return moment(this.workorder.confirm_date).format("DD-MM-YYYY");
    },
    vencimiento:function(){
        return this.workorder.due_date
            && moment(this.workorder.due_date).format('MM-YYYY')
            || false
    },
    thickness_wall:function(){
        return myworkorder.get().thickness_wall()
    },
    thickness_bottom:function(){
        return myworkorder.get().thickness_bottom()
    },
    total_deformation:function(){
        return myworkorder.get().total_deformation()
    },
    cylinder_volume: function () {
        return myworkorder.get().cylinder_volume()
    },
    cm3_reading_max:function(){
        return myworkorder.get().cm3_reading_max()
    },
    balancing_factor:function(){
        return myworkorder.get().balancing_factor()
    },
    cm3_reading_final:function(){
        return myworkorder.get().cm3_reading_final()
    },
    deformation_test:function(){
        return myworkorder.get().deformation_test()
    },
    roundness_test:function(){
        return myworkorder.get().roundness_test()
    },
    thickness_test:function(){
        return myworkorder.get().thickness_test()
    }

})

Template.certificatepdf.events({
    'click #printWorkOrder':function(e){
        e.preventDefault()
         $( "#certificate" ).print();

    },
    'click .tocertificated':function(e){
        e.preventDefault()

        Meteor.call('certificatedWorkOrder',this.workorder._id,function(error){
            if(error)alertify.error(error);
            else alertify.success('Enviado a Certificados Terminados')
        })
    },
    'click #send':function(e){
        e.preventDefault()
       Meteor.call('generarpdf',this.workorder._id,function(err,data){
           console.log('err',err,'data',data);
       })
    }
})

