/**
 * Created by mario on 21/03/16.
 */



selectedcustomerid = new ReactiveVar(null)
cylindersid = new ReactiveVar(null)

Template.workordernewprocess.rendered=function(){
    selectedcustomerid.set( null);
    cylindersid.set(null);
    filtroTubes.set('');
    limitTubes.set(10);
    searchcustomername.set('');
    searchcustomerlimit.set(10);

}

Template.workordernewprocess.helpers({
    cylinders:function(){

        var ids = cylindersid.get();
        return cylindersid.get()  && Cylinders.find({_id:{$in:ids}}) || null;
    },
    lastdoc:function(){
        var id = lastdoc.get()
        var doc = id && WorkOrders.findOne({_id:id._id})
        doc.createdAt = doc && moment(doc.createdAt).format('dddd DD MMM YYYY HH:mm')
        doc.cylinder = doc && Cylinders.findOne({_id:doc.cylinderId})
        doc.customer = doc && doc.cylinder && Customers.findOne({_id:doc.cylinder.customerId})
        $("#codebar").barcode(doc && doc.cylinder && doc.cylinder.cylinder_number, "code128");
        return doc
    }

    /*,
    tasks:function(){
        return _.map(Tasks,function(item){
            return {task:item,value:false}
        })
    }*/
})

Template.workordernewprocess.events({

    'click #newcustomer':function(e){
        e.preventDefault()
        $(".selectcustomer").css('display','none');
        $(".newcustomer").slideToggle(200);
    },
    'click .cancelar':function(e){
        e.preventDefault()
        selectedcustomerid.set(null);
        cylindersid.set(null);
    },
    'click #printBarCode':function(e) {
        e.preventDefault()
        $("#bcTarget").barcode(this.cylinder_number, "code128");
        $(".printable").print();
    },
    'click #generar,#print':function(e,t){
        //e.preventDefault()
        setTimeout(function(){
            $("#workorderprint").print()
        },1000)


        //console.log('this',this)
        //console.log('data',this, t.data)
        //console.log('lastdoc.get()',lastdoc.get())
    }
})
lastdoc = new ReactiveVar('')

AutoForm.addHooks('workordersnewprocess', {
    before: {
        insert: function (doc) {
            lastdoc.set(doc);
            return _.extend(doc,{_id:Random.id()})//,companyId:usr.companies[0]._id})
        }
    },
    onSuccess:function(operation, result){

        if(operation==='insert'){
            var doc = lastdoc.get();
            var ids = cylindersid.get();
            for(var i = ids.length; i--;) {
                if(ids[i] === doc.cylinderId) {
                    ids.splice(i, 1);
                }
            }
            cylindersid.set(ids);
            alertify.success('¡Órden generada!');
        }
    },
    onError: function(formType, error) {

        if(error.error===409)
            alertify.error("Código Duplicado")
        else
            alertify.error(error.message   )

    }

})