Template.alert.helpers({
    customer:function(){
        var customer = Customers.findOne({_id:this.customerId})
        return customer && customer.name + "   " + customer.email || 'Sin Cliente'
    },
    alerts:function(){
        return Alerts.find({cylinderId:this._id})
    },
    createdAt:function(){
        return ReactiveFromNow(this.createdAt)
    }
})
Template.alert.events({
    'click #send':function(e){
        e.preventDefault()
        Meteor.call('sendAlert',this,function(err,result){
            if(err)alertify.error(err.reason)
            else console.log(result)
        })
    },
    'click #cancelalerts':function(e){
        e.preventDefault()
        var itemId=this._id
        var message="¿Quiere cancelar las alertas para este cilindro?"
        alertify.confirm( message, function (confirm) {
            if (confirm) {
                //console.log('rol',rol)
                Meteor.call('cancelAlerts',itemId,function(err){
                    if(err) alertify.error(err.reason)
                    //else alertify.success('Eliminado');
                    //else alertify.success('Realizado');var
                })
            }
        }).set('defaultFocus', 'cancel');
    }

})
