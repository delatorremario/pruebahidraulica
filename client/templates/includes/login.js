/**
 * Created by mario on 10/08/15.
 */


Template.login.events({
      'submit form': function (event) {
        event.preventDefault()
        //console.log('entra')
        var email = event.target.email.value;
        var pws = event.target.password.value;
        Meteor.loginWithPassword(email, pws, function (error) {
          //console.log('entra')
          if (error) {
            alertify.error(error.reason);
          } else {
            var currentRoute = Router.current().route.getName();
            if (currentRoute == "login") {

              Router.go("dashboard");
            }
          }
        });
        //console.log('sale')
      },
      'click #loginGoogle': function(e){
          Meteor.loginWithGoogle({requestPermissions: ['email']},function(err){
            if(err) alertify.error(err.reason)
          })
      }
    }
)
accountsUIBootstrap3.setLanguage('es');

Template._loginButtonsLoggedInDropdown.events({
  'click #login-buttons-edit-profile': function(event) {
    Router.go('profileEdit');
  }
});

accountsUIBootstrap3.logoutCallback = function(error) {
  if(error) console.log("Error:" + error);
  Router.go('login');
}

Accounts.ui.config({
  forceEmailLowercase: true //,
  //forceUsernameLowercase: true,
  //forcePasswordLowercase: true
});

Accounts.ui.config({
  requestPermissions: {},
  extraSignupFields: [{
    fieldName: 'first-name',
    fieldLabel: 'Nombre',
    inputType: 'text',
    visible: true,
    validate: function(value, errorFunction) {
      if (!value) {
        errorFunction("Por favor ingrese su nombre");
        return false;
      } else {
        return true;
      }
    }
  }, {
    fieldName: 'terms',
    fieldLabel: 'Acepta términos y condiciones.',
    inputType: 'checkbox',
    visible: true,
    saveToProfile: false,
    validate: function(value, errorFunction) {
      if (value) {
        return true;
      } else {
        errorFunction('Debe aceptar los términos y condiciones.');
        return false;
      }
    }
  }]
});



Template.register.events({
  'submit .register-form': function (event) {

    event.preventDefault();

    var email = event.target.email.value;
    var password = event.target.password.value;
    var firstname = event.target.firstname.value;
    //var lastname = event.target.lastname.value;

    var user = {email:email,password:password,profile:{name:firstname}};

    Meteor.call('crearUsuario',user,function(err,response){
      //console.log('response',response)
      if(!err) {
        alertify.alert('¡Bienvenido!')
        Router.go('/')
        /*Meteor.loginWithPassword(email, password, function (error) {
          if (error) {
            alertify.error(error.reason);
          } else {
            Router.go('dashboard')

          }
        });*/
      }else{
        alertify.error(err.message)
      }
    })


  }
});
Template.recovery.events({
  'submit .recovery-form': function (e,t) {

    e.preventDefault()

    var email = t.find('#email').value // trimInput(t.find('#email').value)


    Accounts.forgotPassword({ email: email}, function(err){
      if (err)
        alertify.error('Error al enviar el email. Doh!')
      else {
        alertify.alert('Email enviado. Por favor revise su correo')
        Router.go("/")
      }

    });
    //}
    return false;

  }


});



