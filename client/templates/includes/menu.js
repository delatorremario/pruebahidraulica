Template.menu.events({
  'click [data-logout]': function (event, template) {
    event.preventDefault()

    Meteor.logout(function () {
      Router.go('root')
    })
  }
})

$(document).on('click', function () {
  $('.navbar-fixed-top .navbar-collapse.in').collapse('hide')
})
