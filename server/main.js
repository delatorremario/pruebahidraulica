Meteor.startup(function () {
  moment.locale('es')
  //Import.start()
  Meteor.users.createInitialUser()
  Start.start()

  //Start.addMeToAdmin()
  process.env.MAIL_URL ='smtp://postmaster@sandboxe297c51c2a9a4bd9940392455373b5aa.mailgun.org:252a43979ef90b28d7302dfe9f66cf57@smtp.mailgun.org:587'

      //'smtp://cobranzas@carbomensa.com.ar:c@rbomen2016@mail.carbomensa.com.ar:25'  //'smtp://delatorremario@gmail.com:MkujwJs8NOfZIknBOv6Dyg@smtp.mandrillapp.com:587'
  //TODO cambiar a los datos de carbomen

})

var moment = require('moment')
Meteor.settings.contactForm = {
  emailTo: 'info@carbomen.com.ar',


};
Meteor.methods({
  sendAlert:function(cylinder){

    var customer = cylinder && Customers.findOne({_id:cylinder.customerId})
    if(customer){

      /*Email.options =
       {
       from: 'info@carbomen.com.ar',
       siteName: 'CARBOMEN APP',
       companyAddress: '',
       companyName: 'CARBOMEN S.A.',
       companyUrl: 'http://carbomen.com.ar'
       }*/
      var html="<h3>Estimado " + customer.name + "</h3>"
      html+='<h2>Su Cilindro N°' + cylinder.cylinder_number + ' Clase:' + cylinder.class + '</h2>'


      if(moment(cylinder.due_date)>moment(new Date())){
        if(moment(cylinder.due_date)<=moment(new Date()).add(1,'month'))
          var state='¡Por Vencer!'
      }else var state ='¡VENCIDO!'

      html+="<h1>Se encuentra " + state + "</h1>"
      html+="<h1>Fecha de Vencimiento: " + moment(cylinder.due_date).format('MMM-YYYY') + "</h1>"
      html+="<p>CARBOMEN S.A</p>"
      var email ={
        to:customer.email
        //,cc:'delatorremario@gmail.com'
        ,from:'CARBOMEN <info@carbomen.com.ar>'
        ,subject:'Alerta de Vencimiento'
        ,html:html
      }
      this.unblock()
      Email.send(email)
      var message={
        createdAt : new Date(),
        userId:this.userId,
        cylinderId:cylinder._id,
        alert:state

      }
      return Alerts.insert(message)

    }
    else return
  }
})