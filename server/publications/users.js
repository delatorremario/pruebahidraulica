/**
 * Created by mario on 06/01/16.
 */
Meteor.publish('users',function(){
    console.log('subcribe users')

    return Meteor.users.find()
})


Meteor.methods({
    crearUsuario:function(user){

        return Accounts.createUser(user)
        /*if(id){
            var usr = Meteor.users.findOne({_id:id})
            Roles.addUsersToRoles(usr,['boss'])
            var company  = Companies.findOne({name: 'Demo'})
            var companies = [company]
            Meteor.users.update({_id: id},
                {$pushAll: {'companies': companies}},
                {upsert: true})
        }else return*/


    },
    cambiarNombre:function(id,nombre){
        return  Meteor.users.update({_id:id},{$set: {'profile.name':nombre}})
    },
    verificarEmail:function(id){

        return  Meteor.users.update({_id:id},{$set: {'emails.0.verified':true}})
    },
    removeUser:function(id){
        return Meteor.users.remove({_id:id})
    }
})


