/**
 * Created by mario on 01/02/16.
 */
Meteor.publish('customers',function(){
  // console.log('subs customers')
   return Customers.find({})
})

Meteor.publish('customerbyid',function(id){
   return Customers.find({_id:id})
})

Meteor.publish('customersbyname',function(name,limit){
//   console.log('subs customersbyname',name,limit)
   if(name)
      return Customers.find({name: {$regex: name, $options: 'i'}},{sort:{name:1},limit:limit})
   else
      return Customers.find({},{sort:{name:1},limit:limit})
})
