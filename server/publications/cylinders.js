/**
 * Created by mario on 06/02/16.
 */
Meteor.publish('cylinders',function(){
    return Cylinders.find({})
})
Meteor.publish('cylindersforalerts',function(){
    var date = moment(new Date()).add(1,'month').format('MM/DD/YYYY')
    return Cylinders.find({due_date:{$lte:new Date(date)},due_date:{$exists:true}})
})
Meteor.publish('cylindersbycustomerid',function(customerId){
    if(customerId)
        return Cylinders.find({customerId:customerId});
    else
    return []
})

Meteor.publish('cylindersbycustomer',function(){
    var userId = this.userId
    var user = Meteor.users.findOne({_id:userId})
    var customer = Customers.findOne({email:user.emails[0].address})
    if (customer){
        return Cylinders.find({customerId:customer._id});
    }else{
        return []
    }



})
