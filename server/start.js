/**
 * Created by mario on 06/01/16.
 */

Start = {
    start:function(){

        Start.initRoles()
        Start.addMeToAdmin()
    },
    initRoles:function(){
        var roles = Roles.getAllRoles();

        if(roles.count()===0){
            Roles.createRole('admin');
            Roles.createRole('compras');
            Roles.createRole('encargado');
            Roles.createRole('proveedor');
            Roles.createRole('viajante');
        }

    },
    addMeToAdmin:function(){
        if(Meteor.users.find({'emails.address':'delatorremario@gmail.com'}).count()>0){

            var usr = Meteor.users.findOne({'emails.address':'delatorremario@gmail.com'})
            console.log('user agreado a admin',usr)
            Roles.addUsersToRoles(usr,['admin'])
        }
    }
}