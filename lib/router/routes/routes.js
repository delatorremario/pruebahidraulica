/**
 * Created by mario on 07/01/16.
 */
Router.route('/dashboard',               { name: 'dashboard' })
Router.route('/users',                   { name: 'users' })
Router.route('/roles',                   { name: 'roles' })
Router.route('/customers/new',           { name: 'customersnew' })
Router.route('/customers',               { name: 'customers' })
Router.route('/customers/edit/:_id',     { name: 'customersedit' })
Router.route('/cylinders/new',               { name: 'cylindersnew' })
Router.route('/cylinders',                   { name: 'cylinders' })
Router.route('/cylinders/edit/:_id',         { name: 'cylindersedit' })
Router.route('/workorders/new',               { name: 'workordersnew' })
Router.route('/workorders',                   { name: 'workorders' })
Router.route('/workorders/edit/:_id',         { name: 'workordersedit' })
Router.route('/workordersoperador',                   { name: 'workordersoperador' })
Router.route('/workordernewprocess',    {name:'workordernewprocess'})
Router.route('/workordersfinished',    {name:'workordersfinished'})
Router.route('/certificatepdf/:_id',   {name:'certificatepdf'})
Router.route('/alerts',                {name:'alerts'})
Router.route('/invoices',              {name: 'invoices'})

//cliente
Router.route('/pruebashidraulicas',    {name:'pruebashidraulicas'})
Router.route('/miscilindros',          {name:'miscilindros'})

Router.route('/register', {name:'register'})
Router.route('/recovery', {name:'recovery'})

