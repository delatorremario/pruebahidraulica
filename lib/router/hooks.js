mustBeSignedIn = function () {
  if (! Meteor.user()) {
    if (Meteor.loggingIn() && Meteor.status().status === 'connected')
      this.render('loading')
    else
      this.render('login')
  } else {
    this.next()
  }
}

//Router.onBeforeAction('loading')
Router.onBeforeAction(function () {
  if (! Meteor.user()) {
    if (Meteor.loggingIn() && Meteor.status().status === 'connected')
      this.render('loading')
    else
      this.render('login')
  } else {
    this.next()
  }
},{except: ['register','recovery']})
