/**
 * Created by mario on 01/02/16.
 */
var subs = new SubsManager({ cacheLimit: 30 })
CylindersController = RouteController.extend({
    waitOn: function () {
        var waitOn = []
        if (Meteor.status().connected) {
            waitOn.push(
                subs.subscribe('customers'),
                subs.subscribe('workorders'),
                subs.subscribe('cylinders')
            )
        }

        return waitOn
    },
    data:function(){
        var filter = filtroTubes.get()
        var limit = limitTubes.get()

        var items = Cylinders.find({
                $or: [
                    {cylinder_number: {$regex: filter, $options: 'i'}}
                    //, {cylinder: {$regex: filter, $options: 'i'}}
                    //, {description: {$regex: filter, $options: 'i'}}
                ]
            }
            ,{sort:{cylinder_number:1},limit:limit,reactive:true})
        return {items:items}
    }
})

CylindersnewController = RouteController.extend({
    waitOn: function () {
        var waitOn = []
        if (Meteor.status().connected) {
            waitOn.push(
                subs.subscribe('customers'),
                subs.subscribe('cylinders')
            )
        }

        return waitOn
    },
    data: function () {
        var item={}// OrdenesProduccion.findOne({_id: id});
        return {
            formType :'insert',
            item : item,
            omitFields : '_id,userId,createdAt,due_date,comments,cancel_alerts,makeph' //,updatedAt,numero_solicitud,comentarios,inicio_proceso,confirma_proceso'
        }
    }
})

CylinderseditController = RouteController.extend({
    waitOn: function () {
        var waitOn = []
         if (Meteor.status().connected) {
            waitOn.push(
                subs.subscribe('customers'),
                subs.subscribe('cylinders')
            )
        }

        return waitOn
    },
   data: function () {

        var item=Cylinders.findOne(this.params._id);
        return {
            formType :'update',
            item : item,
            omitFields : '_id,userId,createdAt,companyId,updatedAt,approved,comments,due_date,makeph'
        }
    }
})