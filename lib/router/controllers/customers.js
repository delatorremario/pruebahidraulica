/**
 * Created by mario on 01/02/16.
 */
var subs = new SubsManager({ cacheLimit: 30 })
CustomersController = RouteController.extend({
    waitOn: function () {
        var waitOn = []
        //console.log('subscribe in ItemsnewController',searchcustomername.get(),searchcustomerlimit.get())
        if (Meteor.status().connected) {
            waitOn.push(
                subs.subscribe('users'),
                subs.subscribe('customersbyname',searchcustomername.get(),searchcustomerlimit.get())
            )
        }
        return waitOn
    },
    data: function () {
        var filter = searchcustomername.get()
        var limit = searchcustomerlimit.get()


        var customers = Customers.find({
                $or: [
                    {name: {$regex: filter, $options: 'i'}}
                    //, {cylinder: {$regex: filter, $options: 'i'}}
                    //, {description: {$regex: filter, $options: 'i'}}
                ]
            }
            ,{sort:{name:1},limit:limit,reactive:true})
        return {customers:customers}

        //return { customers: Customers.find({},{sort:{name:1},reactive:true})}
    }


})

CustomersnewController = RouteController.extend({
    waitOn: function () {
        var waitOn = []
        console.log('subscribe in ItemsnewController')
        if (Meteor.status().connected) {
            waitOn.push(
                subs.subscribe('users'),
                subs.subscribe('customers')
            )
        }

        return waitOn
    },
    data: function () {
        var item={}// OrdenesProduccion.findOne({_id: id});
        return {
            formType :'insert',
            item : item,
            omitFields : '_id,userId,createdAt,companyId,updatedAt'
        }
    }
})

CustomerseditController = RouteController.extend({
    waitOn: function () {
        var waitOn = []
        console.log('subscribe in ItemsnewController')
        if (Meteor.status().connected) {
            waitOn.push(
                subs.subscribe('users'),
                subs.subscribe('customers')
            )
        }

        return waitOn
    },
   data: function () {

        var item=Customers.findOne(this.params._id);
        return {
            formType :'update',
            item : item,
            omitFields : '_id,userId,createdAt,companyId,updatedAt' //,updatedAt,numero_solicitud,comentarios,inicio_proceso,confirma_proceso'
        }
    }
})