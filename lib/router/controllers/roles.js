/**
 * Created by mario on 06/01/16.
 */
var subs = new SubsManager({ cacheLimit: 30 })
RolesController = RouteController.extend({
    waitOn: function () {
        var waitOn = []
        console.log('subscribe in DashboardController')
        if (Meteor.status().connected) {
            waitOn.push(
                subs.subscribe('users'),
                subs.subscribe('roles')
            )
        }

        return waitOn
    },
    data: function () {
        //console.log('usuarios',Meteor.users.find({}).fetch())
        return { roles:Roles.getAllRoles()}
    }
})