/**
 * Created by mario on 11/04/16.
 */
var subs = new SubsManager({ cacheLimit: 30 })
MiscilindrosController = RouteController.extend({
    waitOn: function () {
        //console.log('waitOn')
        var waitOn = []
        if (Meteor.status().connected) {
            waitOn.push(
                //subs.subscribe('users'),
                subs.subscribe('cylindersbycustomer')
            )
        }
        //console.log('waitOn',waitOn)
        return waitOn
    },
    data:function(){
        //console.log('data')
        var filter = filtroTubes.get()
        var limit = limitTubes.get()

        var items = Cylinders.find({
                $or: [
                    {cylinder_number: {$regex: filter, $options: 'i'}}
                    //, {cylinder: {$regex: filter, $options: 'i'}}
                    //, {description: {$regex: filter, $options: 'i'}}
                ]
            }
            ,{sort:{approved:-1, due_date:1, cylinder_number:1},limit:limit,reactive:true})
        return {cilindros:items}
    }
})