var subs = new SubsManager({ cacheLimit: 30 })
InvoicesController = RouteController.extend({
    waitOn: function () {
        var waitOn = []
        if (Meteor.status().connected) {
            waitOn.push(
                subs.subscribe('roles'),
                subs.subscribe('customers'),
                subs.subscribe('cylinders'),
                subs.subscribe('workorders')
            )
        }
        return waitOn
    },//TODO hacer que funcione este
    /*onBeforeAction: function () {
     if (!Roles.userIsInRole(Meteor.userId(), ['admin,encargado'])){
     alertify.warning('Usted no tiene permisos para continuar')
     Router.go('/');}
     else
     this.next();
     },*/
    data:function(){
        return {items:WorkOrders.find({$and:[{confirm_date:{$exists:true}},{invoice_date:{$exists:false}}]}
            ,{sort:{customerId:-1},reactive:true})}
    }
})
