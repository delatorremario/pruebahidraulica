/**
 * Created by mario on 06/01/16.
 */
var subs = new SubsManager({ cacheLimit: 30 })
UsersController = RouteController.extend({
    waitOn: function () {
        var waitOn = []
        console.log('suscribe in UsersController')
        if (Meteor.status().connected) {
            waitOn.push(
                subs.subscribe('users'),
                subs.subscribe('roles')

            )
        }

        return waitOn
    },
    data: function () {
        //console.log('usuarios',Meteor.users.find({}).fetch())
        return { users:  Meteor.users.find({}, { sort: { 'profile.name': 1 } }),
                 roles: Meteor.roles.find({})
        }
    }
})