/**
 * Created by mario on 11/04/16.
 */
var subs = new SubsManager({ cacheLimit: 30 })
AlertsController = RouteController.extend({
    waitOn: function () {
        //console.log('waitOn')
        var waitOn = []
        if (Meteor.status().connected) {
            waitOn.push(
                //subs.subscribe('users'),
                subs.subscribe('customers'),
                subs.subscribe('alerts'),
                subs.subscribe('cylindersforalerts')
            )
        }
        //console.log('waitOn',waitOn)
        return waitOn
    },
    data:function(){
        var date = moment(new Date()).add(1,'month').format('MM/DD/YYYY')

        return {cylinders:Cylinders.find({
             $and:[
                 {due_date:{$exists:true}},
                 {due_date:{$lte:new Date(date)}},
                 {cancel_alerts:false}
             ]
            },{sort:{due_date:-1},reactive:true})}
    }
})