/**
 * Created by mario on 11/02/16.
 */
var subs = new SubsManager({ cacheLimit: 30 })
WorkordersController = RouteController.extend({
    waitOn: function () {
        var waitOn = []
        if (Meteor.status().connected) {
            waitOn.push(
                subs.subscribe('roles'),
                subs.subscribe('customers'),
                subs.subscribe('cylinders'),
                subs.subscribe('workorders')
            )
        }
        return waitOn
    },//TODO hacer que funcione este
    /*onBeforeAction: function () {
    if (!Roles.userIsInRole(Meteor.userId(), ['admin,encargado'])){
        alertify.warning('Usted no tiene permisos para continuar')
        Router.go('/');}
    else
        this.next();
    },*/
    data:function(){
        var filter = filtroWO.get()
        var limit = limitWO.get()
        //console.log('filter',filter)

        var items = WorkOrders.find({$and:[{confirm_date:{$exists:true}},{sent:{$exists:false}}]
            /*$or: [

             { workorder_number: { $gt: filter } }
             //{workorder_number: {$regex: filter, $options: 'i'}}
             //, {description: {$regex: filter, $options: 'i'}}
             ]*/
        },{sort:{ customerId:1, createdAt:-1},limit:limit,reactive:true})
        return {items:items}
    }
})
WorkordersoperadorController = RouteController.extend({
    waitOn: function () {
        var waitOn = []
        if (Meteor.status().connected) {
            waitOn.push(
                subs.subscribe('customers'),
                subs.subscribe('cylinders'),
                subs.subscribe('workordersoperador')
            )
        }

        return waitOn
    },
    data:function(){
        return {items:WorkOrders.find({confirm_date:{$exists:false}},{reactive:true})}
    }
})

WorkordersnewController = RouteController.extend({
    waitOn: function () {
        var waitOn = []
        if (Meteor.status().connected) {
            waitOn.push(
                subs.subscribe('customers'),
                subs.subscribe('cylinders')
            )
        }

        return waitOn
    },
    data: function () {
        var item={}// OrdenesProduccion.findOne({_id: id});
        return {
            formType :'insert',
            item : item,
            omitFields : '_id,userId,createdAt,workorder_number,cylinderId,stamp,sent,due_date,approved' //,updatedAt,numero_solicitud,comentarios,inicio_proceso,confirma_proceso'
        }
    }
})


WorkorderseditController = RouteController.extend({
    waitOn: function () {
        var waitOn = []
        if (Meteor.status().connected) {
            waitOn.push(
                subs.subscribe('customers'),
                subs.subscribe('cylinders'),
                subs.subscribe('workorders')
            )
        }

        return waitOn
    },
    data: function () {

        var item=WorkOrders.findOne(this.params._id);
        item.cylinder=Cylinders.findOne(item.cylinderId)
        myworkorder.set(new WorkOrder(item));
        return {
            formType :'update',
            item : item,
        }
    }
})

WorkordernewprocessController=RouteController.extend({
    waitOn:function(){
        var waitOn=[]
        if(Meteor.status().connected){
            waitOn.push(
                subs.subscribe('customerbyid',selectedcustomerid.get()),
                subs.subscribe('customersbyname',searchcustomername.get(),searchcustomerlimit.get()),
                subs.subscribe('cylindersbycustomerid',selectedcustomerid.get()),
                subs.subscribe('workordersbyid',lastdoc.get()._id)
            )
        }
    },
    data:function(){
        var filter = filtroTubes.get()
        var limit = limitTubes.get()

        var items = Cylinders.find({
                $and: [
                    {cylinder_number: {$regex: filter, $options: 'i'}}
                    , {customerId: selectedcustomerid.get() }
                    //, {description: {$regex: filter, $options: 'i'}}
                ]
            }
            ,{sort:{cylinder_number:1},limit:limit,reactive:true})

        var filtercustomer = searchcustomername.get()
        var limitcustomer = searchcustomerlimit.get()


        var customers = Customers.find({
                $or: [
                    {name: {$regex: filtercustomer, $options: 'i'}}
                    //, {cylinder: {$regex: filter, $options: 'i'}}
                    //, {description: {$regex: filter, $options: 'i'}}
                ]
            }
            ,{sort:{name:1},limit:limitcustomer,reactive:true})
         return {
            selectedcustomer:Customers.findOne({_id:selectedcustomerid.get()}),
            items:items,
            customers:customers
        }
    }
})



WorkordersfinishedController = RouteController.extend({
    waitOn: function () {
        var waitOn = []
        if (Meteor.status().connected) {
            waitOn.push(
                subs.subscribe('customers'),
                subs.subscribe('cylinders'),
                subs.subscribe('workorders')
            )
        }
        return waitOn
    },
    data:function(){
        var filter = filtroWO.get()
        var limit = limitWO.get()
        //console.log('filter',filter)

        var items = WorkOrders.find({sent:{$exists:true}
            /*$or: [

             { workorder_number: { $gt: filter } }
             //{workorder_number: {$regex: filter, $options: 'i'}}
             //, {description: {$regex: filter, $options: 'i'}}
             ]*/
        },{sort:{ customerId:1, createdAt:-1},limit:limit,reactive:true})
        return {items:items}
    }
})