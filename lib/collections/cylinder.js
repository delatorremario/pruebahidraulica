/**
 * Created by mario on 06/02/16.
 */
//Pedidos = new Ground.Collection( new Mongo.Collection('pedidos'),{version:1.0})
Cylinders = new Ground.Collection( new Mongo.Collection('cylinders'),{version:1.0})
Cylinders.attachSchema(Schemas.Cylinder)

Cylinders.allow({
    insert: function (doc) {
        return !!doc
    },
    update: function (doc) {
        return !!doc
    }
})

Meteor.methods({
    deleteTube:function(itemId){
        return Cylinders.remove({_id:itemId})
    },
    tubesCount:function(){
        return Cylinders.find().count()
    },
    addcomment:function(id,comment){
        return Cylinders.update({_id:id},{$pushAll:{comments:[comment]}})
    }
})
