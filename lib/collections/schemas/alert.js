/**
 * Created by mario on 06/02/16.
 */
Schemas.Alert = new SimpleSchema([
    Schemas.Base,
    { cylinderId:{type:String}},
    { alert:{type:String}}
])