/**
 * Created by mario on 06/02/16.
 */
Schemas.Cylinder = new SimpleSchema([
    Schemas.Base,
    {customerId:{type:String
        ,autoform:{
            label: function() { return TAPi18n.__('customer'); },
            placeholder: function(){ return TAPi18n.__('customer'); },
            firstOption:function () { return 'Seleccione un ' + TAPi18n.__('customer') },
            options:function(){
                return  Customers.find({},{sort:{name:1}}).map (function(doc){
                    return {
                        label:doc.name,
                        value:doc._id
                    }
                })
            }
        }
    }},
    {cylinder_number:{type:String,
        autoform:{
            label: function() {return TAPi18n.__('cylinder_number');},
            placeholder: function(){return TAPi18n.__('cylinder_number');}
            }
        }},
    {manufacturing_date:{type:Date,optional:true
        ,autoform:{
        //type:"bootstrap-datepicker",
        label: function() {return TAPi18n.__('manufacturing_date');},
        placeholder:function() {return TAPi18n.__('manufacturing_date');}
    }}},
    {class:{type:String,optional:true,
        autoform:{
            label: function() {return TAPi18n.__('class');},
            placeholder: function(){return TAPi18n.__('class');},
            firstOption:function () { return 'Seleccione una ' + TAPi18n.__('class') },
            options:function(){
                return [
                    {label:'O2',value:'O2'},
                    {label:'N2',value:'N2'},
                    {label:'CO2',value:'CO2'},
                    {label:'ARGON',value:'ARGON'},
                    {label:'ETIL',value:'ETIL'},
                    {label:'GAS-MEZCLA',value:'GAZ-MEZCLA'},
                    {label:'O2 MED',value:'O2 MED'},
                    {label:'AIRE-COMPRIMIDO',value:'AIRE-COMPRIMIDO'},
                    {label:'CONCERVARE',value:'CONCERVARE'},
                    {label:'FORMIER',value:'FORMIER'},
                    {label:'HELIO',value:'HELIO'}

                ]
            }
        }
    }},
    {capacity:{type:Number,optional:true,decimal:true,
        autoform:{
        label: function() {return TAPi18n.__('capacity');},
        placeholder: function(){return TAPi18n.__('capacity');},
            firstOption:function () { return 'Seleccione una ' + TAPi18n.__('capacity') },
            options:function(){
                return [
                    {label:'0.5',value:0.5},
                    {label:'1',value:1},
                    {label:'1.5',value:1.5},
                    {label:'2',value:2},
                    {label:'3',value:3},
                    {label:'4',value:4},
                    {label:'5',value:5},
                    {label:'6',value:6},
                    {label:'7',value:7},
                    {label:'8',value:8},
                    {label:'9',value:9},
                    {label:'10',value:10}
                ]
            }
    }
    }},
    {type_base:{type:String,optional:true,
        autoform:{
        label: function() {return TAPi18n.__('type_base');},
        placeholder: function(){return TAPi18n.__('type_base');},
            firstOption:function () { return 'Seleccione una ' + TAPi18n.__('type_base') },
            options:function(){
                return [
                    {label:'CONCAVO',value:'CONCAVO'},
                    {label:'CONVEXO',value:'CONVEXO'}
                ]
            }
    }
    }},
    {test_pressure:{type:Number,optional:true,
        autoform:{
        label: function() {return TAPi18n.__('test_pressure');},
        placeholder: function(){return TAPi18n.__('test_pressure');},
            firstOption:function () { return 'Seleccione una ' + TAPi18n.__('test_pressure') },
            options:function(){
                return [
                    {label:'225',value:225},
                    {label:'250',value:250},
                    {label:'260',value:260},
                    {label:'275',value:275},
                    {label:'300',value:300},
                    {label:'310',value:310},
                    {label:'320',value:320},
                    {label:'330',value:330},
                    {label:'375',value:375},
                    {label:'390',value:390}
                ]
            }
    }
    }},
    {show_mts:{type:Boolean,optional:true,defaultValue:false,
        autoform:{
            leftLabel: true,
            label: function() {return TAPi18n.__('show_mts');},
        }
    }},
    {thickness:{type:Number,optional:true,min:0,
        autoform:{
        label: function() {return TAPi18n.__('thickness');},
        placeholder: function(){return TAPi18n.__('thickness');}
    }
    }},
    {due_date:{type:Date,optional:true}},
    {approved:{type:Boolean,defaultValue:true}},
    {'comments':{type:String,optional:true}},
    {'comments.$':{type:Object,optional:true}},
    {'comments.$.user_name':{type:String,optional:true}},
    {'comments.$.comment':{type:String,optional:true}},
    {'comments.$.date':{type:Date,optional:true}},
    { cancel_alerts:{type:Boolean,defaultValue:false,label:'Cancelar Alertas'}},
    { makeph:{type:Date,optional:true}}

])