/**
 * Created by mario on 11/02/16.
 */
Schemas.Workorder = new SimpleSchema([
    Schemas.Base,
    {workorder_number: {
            type: Number,
            index: true,
            autoValue: function () {
                if(this.isInsert){
                    var ord = WorkOrders.findOne({}, {sort: {workorder_number: -1}})
                    if (ord) return ord.workorder_number + 1
                    else return 1

                    //return WorkOrders.findOne({}, {sort: {workorder_number: -1}}).workorder_number+1 || 1
                }
                else this.unset()
            }
        }
    },
    {observations:{type:String,optional:true,
        label: function() {return TAPi18n.__('observations');}}},
    {roundness_1:{type:Number,decimal:true,optional:true,
        label: function() {return TAPi18n.__('roundness_1');}}},
    {roundness_2:{type:Number,decimal:true,optional:true,
        label: function() {return TAPi18n.__('roundness_2');}}},
    {roundness_3:{type:Number,decimal:true,optional:true,
        label: function() {return TAPi18n.__('roundness_3');}}},
    {roundness_4:{type:Number,decimal:true,optional:true,
        label: function() {return TAPi18n.__('roundness_4');}}},
    {roundness_5:{type:Number,decimal:true,optional:true,
        label: function() {return TAPi18n.__('roundness_5');}}},
    {roundness_6:{type:Number,decimal:true,optional:true,
        label: function() {return TAPi18n.__('roundness_6');}}},
    {variation_thread:{type:Boolean,defaultValue:false,label: function() {return TAPi18n.__('variation_thread');}}},
    {tasks:{
            type:[String],
            allowedValues: Tasks,
        label: function() {return TAPi18n.__('tasks');}
        }},
    {tasks_finished:{type:[String],optional:true}},
    {cylinderId :{type:String
        ,defaultValue:function(){
            return selectedcylinder.get()
        }
        /*,autoform:{
            firstOption:function () { return 'Seleccione un Cilindro' }, //false,
            options:function(){
                return  Cylinders.find({},{sort:{cylinder_number:1}}).map (function(doc){
                    return {
                        label:doc.cylinder_number + ' | ' + Customers.findOne({_id:doc.customerId}).name ,
                        value:doc._id
                    }
                })
            }
        }*/

    }},
    {work_pressure:{type:Number,optional:true, min:0,
        label: function() {return TAPi18n.__('work_pressure');}
    }},
    {mass:{type:Number,decimal:true,optional:true,min:0,
        label: function() {return TAPi18n.__('mass');}
    }},
    {mass_control:{type:Number,decimal:true,optional:true,min:0,
        label: function() {return TAPi18n.__('mass_control');}
    }},
    {waterfilled_cylinder:{type:Number,decimal:true,optional:true,
        label: function() {return TAPi18n.__('waterfilled_cylinder');}}},
    {burette_reading_max:{type:Number,decimal:true,optional:true,
        label: function() {return TAPi18n.__('burette_reading_max');}
    }},
    {temperature:{type:Number,decimal:true,optional:true,
        label: function() {return TAPi18n.__('temperature');}}},
    {burette_reading_final:{type:Number,decimal:true,optional:true,
        label: function() {return TAPi18n.__('burette_reading_final');}}},
    {thickness_1:{type:Number,decimal:true,optional:true,
        label: function() {return TAPi18n.__('thickness_1');}}},
    {thickness_2:{type:Number,decimal:true,optional:true,
        label: function() {return TAPi18n.__('thickness_2');}}},
    {thickness_3:{type:Number,decimal:true,optional:true,
        label: function() {return TAPi18n.__('thickness_3');}}},
    {thickness_4:{type:Number,decimal:true,optional:true,
        label: function() {return TAPi18n.__('thickness_4');}}},
    //{thickness_5:{type:Number,decimal:true,optional:true,
    //    label: function() {return TAPi18n.__('thickness_5');}}},
    {confirm_date:{type:Date, optional:true}},
    {stamp:{type:String,optional:true}},
    {sent:{type:Date,optional:true}},
    {due_date:{type:Date,optional:true}},
    {approved:{type:Boolean,defaultValue:true}},
    {visual_inspection:{type:Boolean,defaultValue:false,optional:true,autoform:{omit:true}}},
    {invoice_date:{type:Date,optional:true,autoform:{omit:true}}}
])

