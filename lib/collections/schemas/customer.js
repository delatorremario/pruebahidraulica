/**
 * Created by mario on 01/02/16.
 */
Schemas.Customer = new SimpleSchema([
    Schemas.Base,
    //{companyId:{type:String}},
    {name:{type:String,
        autoform:{
            label: function() {return TAPi18n.__('name');},
            placeholder: function(){return TAPi18n.__('name');}
        }
    }},
    {phone:{type:String,optional:true,
        autoform:{
            label: function() {return TAPi18n.__('phone');},
            placeholder: function(){return TAPi18n.__('phone');}
        }
    }},
    {email:{type:String,optional:true, unique:true,
        autoform:{
            label: function() {return TAPi18n.__('email');},
            placeholder: function(){return TAPi18n.__('email');}
        }
    }},
    {address:{type:String,optional:true,
        autoform:{
            label: function() {return TAPi18n.__('address')},
            placeholder: function(){return TAPi18n.__('address')}
        }}
    }
])