/**
 * Created by mario on 01/02/16.
 */
Customers = new Mongo.Collection('customers')
Customers.attachSchema(Schemas.Customer)

Customers.allow({
    insert: function (doc) {
        return !!doc
    },
    update: function (doc) {
        return !!doc
    }
})

Meteor.methods({

})