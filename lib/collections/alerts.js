Alerts = new Ground.Collection( new Mongo.Collection('alerts'),{version:1.0})
Alerts.attachSchema(Schemas.Alert)

Alerts.allow({
    insert: function (doc) {
        return !!doc
    },
    update: function (doc) {
        return !!doc
    }
})



Meteor.methods({
    cancelAlerts:function(cylindirId){
        return Cylinders.update({_id:cylindirId},{$set:{cancel_alerts:true}})
    }/*,
    sendAlert:function(cylinder){

        var customer = cylinder && Customers.findOne({_id:cylinder.customerId})
        if(customer){

            Email.options =
            {
                from: 'info@carbomen.com.ar',
                siteName: 'CARBOMEN APP',
                companyAddress: '',
                companyName: 'CARBOMEN S.A.',
                companyUrl: 'http://carbomen.com.ar'
            }
            var html="<h3>Estimado " + customer.name + "</h3>"
            html+='<h2>Su Cilindro N°' + cylinder.cylinder_number + ' Clase:' + cylinder.class + '</h2>'


                if(moment(cylinder.due_date)>moment(new Date())){
                    if(moment(cylinder.due_date)<=moment(new Date()).add(1,'month'))
                    var state='¡Por Vencer!'
                }else var state ='¡VENCIDO!'

            html+="<h1>Se encuentra " + state + "</h1>"
            html+="<h1>Fecha de Vencimiento: " + moment(cylinder.due_date).format('MMM-YYYY') + "</h1>"
            html+="<p>CARBOMEN S.A</p>"
            var email ={
                to:customer.email
                //,cc:'delatorremario@gmail.com'
                ,from:'CARBOMEN <info@carbomen.com.ar>'
                ,subject:'Alerta de Vencimiento'
                ,html:html
            }
            console.log('email',email)
            this.unblock()
            Email.send(email)
            var message={
                createdAt : new Date(),
                userId:this.userId,
                cylinderId:cylinder._id,
                alert:state

            }
           return Alerts.insert(message)

        }
        else return
    }
    */
})
