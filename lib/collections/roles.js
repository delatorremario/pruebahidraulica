/**
 * Created by mario on 06/01/16.
 */
//Ground.Collection(Meteor.roles)
Meteor.methods({
    createRole:function(rol){
        return Roles.createRole(rol);
    },
    addUserToRol: function (usrId,roles) {
        var usr = Meteor.users.findOne({_id:usrId})
        return Roles.addUsersToRoles(usr,roles)
    },
    removeRol:function(rol){
        return Meteor.roles.remove({name:rol.name})
    },
    removeUserFromRol:function(usr,rol){
        return Roles.removeUsersFromRoles(usr, rol)
    }
})
