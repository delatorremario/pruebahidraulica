/**
 * Created by mario on 11/02/16.
 */
WorkOrders = new Mongo.Collection('workorders')
WorkOrders.attachSchema(Schemas.Workorder)

WorkOrders.allow({
    insert: function (doc) {
        return !!doc
    },
    update: function (doc) {
        return !!doc
    }
})

Meteor.methods({
    confirmWorkOrder:function(id,ph,vencer,approved,cylinderId,visual_inspection){
        //var duration = moment.duration(vencer, 'years');
        //console.log('entra')
        var due_date = moment(new Date()).add(vencer,'years')//.format('DD-MM-YYYY');
        //console.log('duration,due_date',due_date.format('DD-MM-YYYY'));


        if(ph)
            return  WorkOrders.update({_id:id},{$set:{confirm_date:new Date()
                    ,due_date:due_date.format('MM-DD-YYYY')
                    ,approved:approved
                    ,visual_inspection:visual_inspection
                }
                }) && Cylinders.update({_id:cylinderId},{$set:{
                    due_date:due_date.format('MM-DD-YYYY'),
                    approved:approved,
                    makeph:new Date()
                }
                })
        else return  WorkOrders.update({_id:id},{$set:{confirm_date:new Date()}})




    },
    updateTask:function(id,task){
        return WorkOrders.update({_id:id},{$push:{tasks_finished:task}})
    },
    stampWorkOrder:function(id,stamp){
        return  WorkOrders.update({_id:id},{$set:{stamp:stamp}});
    },
    certificatedWorkOrder:function(id){
        return  WorkOrders.update({_id:id},{$set:{sent:new Date()}});
    },
    invoiceWorkOrder:function(id){
        return  WorkOrders.update({_id:id},{$set:{invoice_date:new Date()}});
    },
    generarpdf:function(id){
        var doc = new PDFDocument({size: 'A4', margin: 50});
        //var imageBase64 = Meteor.users.findOne(this.userId).profile.picture;
        //var imageBuffer2 = new Buffer(imageBase64.replace('data:image/png;base64,','') || '', 'base64');
        //doc.image(imageBuffer2, 10, 10, {height: 75});
        doc.fontSize(12);
        doc.text('PDFKit is simple', 10, 30, {align: 'center', width: 200});
        // Save it on myApp/public/pdf folder (or any place) with the Fibered sync methode:
        doc.writeSync(process.env.PWD + '/public/pdf/'+id+'.pdf');

    }
})