module.exports = {
  servers: {
    one: {
      host: '104.131.133.38',
      username: 'root',
      // pem:
      // or leave blank for authenticate from ssh-agent
    }
  },

  meteor: {
    name: 'ph',
    path: '.',
    servers: {
      one: {}
    },
    buildOptions: {
      serverOnly: true,
    },
    env: {
      ROOT_URL: 'http://104.131.133.38',
      MONGO_URL: "mongodb://phcarbomen:phcarbomenpass@ds011810.mlab.com:11810/phcarbomen"
  },

    //dockerImage: 'kadirahq/meteord'
    deployCheckWaitTime: 160,
    /*ssl: {
      // Enables let's encrypt (optional)
      crt: './ssl/bundle.crt', // this is a bundle of certificates
      key: './ssl/privkey.pem', // this is the private key of the certificate
      port: 443
    }*/
  }/*,

  mongo: {
    oplog: true,
    port: 27017,
    servers: {
      one: {},
    },
  },*/
};
